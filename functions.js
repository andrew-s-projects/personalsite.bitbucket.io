const { monitorEventLoopDelay } = require("perf_hooks");


function displayML(){
    document.getElementById("AIProjects").style.display = "none";
    document.getElementById("otherProjects").style.display = "none";
    document.getElementById("groupProjects").style.display = "none";
    document.getElementById("machinelearningProjects").style.display = "grid";
}
function displayAI(){
    document.getElementById("machinelearningProjects").style.display = "none";
    document.getElementById("otherProjects").style.display = "none";
    document.getElementById("groupProjects").style.display = "none";
    document.getElementById("AIProjects").style.display = "grid";
}
function displayOther(){
    document.getElementById("AIProjects").style.display = "none";
    document.getElementById("machinelearningProjects").style.display = "none";
    document.getElementById("groupProjects").style.display = "none";
    document.getElementById("otherProjects").style.display = "grid";
    
}
function displayGroup(){
    document.getElementById("AIProjects").style.display = "none";
    document.getElementById("machinelearningProjects").style.display = "none";
    document.getElementById("otherProjects").style.display = "none";
    document.getElementById("groupProjects").style.display = "grid";

}
function showGithub(){
    document.getElementById("contentBox").style.backgroundImage = 'url("../site_main/imgs/code.PNG")';
    document.getElementById("codeSlide").style.display = "grid";
    document.getElementById("contactSlide").style.display = "none";
    document.getElementById("projectSlide").style.display = "none";

}
function showContact(){
    document.getElementById("contentBox").style.backgroundImage = "none";
    document.getElementById("contactSlide").style.display = "grid";
    document.getElementById("codeSlide").style.display = "none";
    document.getElementById("projectSlide").style.display = "none";
}
function showProject(){
    document.getElementById("contentBox").style.backgroundImage = 'url("../site_main/imgs/projects.png")';
    document.getElementById("projectSlide").style.display = "grid";
    document.getElementById("contactSlide").style.display = "none";
    document.getElementById("codeSlide").style.display = "none";
    
}


function showProjects(){
    $('html, body').animate({
        scrollTop: $(".About").offset().top}, 'slow');
}
function readMore(the_id){
    var the_element = the_id +"modal";
    document.getElementById(the_element).style.display = "block";

    window.addEventListener("click", function(event){

        if(event.target.id == the_element){
            document.getElementById(the_element).style.display = "none";
        }
    });

}
function displayInfo(){
    document.getElementById("infoModal").style.display= "block";

    var span = document.getElementsByClassName("close")[0];

    var modal = document.getElementById("infoModal");

    span.onclick = function(){
        document.getElementById("infoModal").style.display = "none";
    }
    window.addEventListener("click", function(event) {
        
        if(event.target == modal){
            document.getElementById("infoModal").style.display = "none";
        }
    });
}

function changeShowcase(slide){
    var temp = slide + 1;

    if(temp == 1){
       showGithub();
    }
    if(temp == 2){
        showContact();
    }
    if(temp > 3){
        showProject();
        slide = -1;
    }
    slide = slide + 1;
    setTimeout(changeShowcase, 8000, slide);
}


